# magnetic_field_map_msr

## Contact

Contact Tom Shelton (thomas.shelton@uky.edu or --soon-- ts21@illinois.edu) for questions etc.  Or Chris Crawford (c.crawford@uky.edu).

## Requirenments

Model made in COMSOL 5.5 with the AC/DC module only.

## Acknowledgements

TODO.

## Copyright and license

All rights reserved and TODO.

## magnetic_field_map_msr documentation

This is a COMSOL model for a global simulation of a magnetic field map in and around the six-layer magnetic shield room (MSR) of the n2EDM experiment.


TODO details